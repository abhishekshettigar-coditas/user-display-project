// getting player data
const getPlayersName = async () => {
  try {
    const response = await fetch('/details.json', {
      method: 'GET',
      headers: new Headers({
        "ngrok-skip-browser-warning": "1234",
      })
    });
    const data = await response.json();
    createPlayerList(data);
  } catch (error) {
    console.log(error)
    const errorMessage = `
    <img id="error-message" src="https://blog.hubspot.com/hs-fs/hubfs/404-error-page-mckissack.png?width=975&name=404-error-page-mckissack.png" alt="service unavailable">
    `
    const bodyElement = document.querySelector("body")
    bodyElement.innerHTML = errorMessage;
  }
}
getPlayersName();

// create player list
const createPlayerList = (data) => {
  data.forEach(playerData => {
    const playerList = document.querySelector('.player-list')
    const playerName = document.createElement('BUTTON');

    playerList.appendChild(playerName);
    playerName.classList.add('player')
    playerName.innerText = playerData.name;
    playerName.setAttribute('id', `${playerData.id}`);
    playerList.addEventListener('click', () => selectPlayer(playerData))
  })
}

// select player
const selectPlayer = (playerData) => {
  const playerList = document.querySelector('.player-list');
  const selectedPlayerID = event.target.id;

  if (selectedPlayerID) {
    const players = document.querySelectorAll('.player');
    players.forEach((player) => {
      player.classList.remove('player-live');
    })
    event.target.classList.add('player-live');
  }
  return displayPlayers(playerData);
}

// display player
const displayPlayers = (playerData) => {
  const selectedPlayerID = event.target.id;

  if (Number.parseInt(selectedPlayerID) === playerData.id) {
    const div = document.createElement('DIV');
    const detailsSection = document.querySelector('.player-details')

    const template = `
          <h2 class="player-name">${playerData.name}</h2>

          <section class="player-details-content">
              <div class="player-attribute">
                  <span class="attribute">Nick Name: ${playerData.nickName}</span>
                  <span class="attribute">Age: ${playerData.age}</span>
                  <span class="attribute">Batting Style: ${playerData.battingStyle}</span>
                  <span class="attribute">Bowling Style: ${playerData.bowlingStyle}</span>
                  <span class="attribute">Awards: ${playerData.awards}</span>
                  <p class="attribute">About: ${playerData.about}</p>
                  <a class="attribute" href=${playerData.instagramHandle}" target="_blank">Instagram</a>
              </div>
              <div class="player-image">
                  <img src=${playerData.imgPath}
                      alt="player image">
              </div>
          </section>`;

    div.innerHTML = template;

    detailsSection.innerHTML = '';
    detailsSection.appendChild(div);
  }
}